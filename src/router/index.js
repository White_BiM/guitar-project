import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import PersonalAccount from '@/components/Auth/PersonalAccount'
import Bonus from '@/components/Bonus/Bonus'
import BonusesCatalog from '@/components/Bonus/BonusesCatalog'
import Lesson from '@/components/Lessons/Lesson'
import LessonsCatalog from '@/components/Lessons/LessonsCatalog'
import Stream from '@/components/Streams/Stream'
import StreamsCatalog from '@/components/Streams/StreamsCatalog'
import UserList from '@/components/UserList'
import AuthGuard  from './auth-guard'
import Forum from '@/components/Forum/Forum'
import Thread from '@/components/Forum/Thread'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/Forum',
            props: true,
            name: 'Forum',
            component: Forum
        },
        {
            path: '/thread/:id',
            props: true,
            name: 'Thread',
            component: Thread
        },
        {
            path: '',
            name: 'Home',
            component: Home
        },
        {
            path: '/UserList',
            name: 'UserList',
            component: UserList
        },
        {
            path: '/PersonalAccount',
            props: true,
            name: 'PersonalAccount',
            component: PersonalAccount,
            beforeEnter: AuthGuard
        },
        {
            path: '/bonus/:id',
            props: true,
            name: 'Bonus',
            component: Bonus
        },
        {
            path: '/bonus-catalog',
            name: 'BonusesCatalog',
            component: BonusesCatalog
        },
        {
            path: '/lesson/:id',
            props: true,
            name: 'Lesson',
            component: Lesson

        },
        {
            path: '/lessons-catalog',
            name: 'LessonsCatalog',
            component: LessonsCatalog
        },
        {
            path: '/stream/:id',
            props: true,
            name: 'Stream',
            component: Stream
        },
        {
            path: '/streams-catalog',
            name: 'StreamsCatalog',
            component: StreamsCatalog
        }
    ],
    scrollBehavior(){
        return{x:0,y:0}
    }
})