import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import LoginModalComponent from '@/components/Shared/LoginModal'
import RegistrationModalComponent from '@/components/Shared/RegistrationModal'
import AddMaterial from '@/components/Auth/AddMaterial'
import UserDataChange from '@/components/Auth/UserDataChange'
import store from './store'
import * as fb from 'firebase'
import userProgress from '@/components/Auth/UserProgress'
import ToCheckModal from '@/components/Shared/ToCheckModal'
import ControlComponent from '@/components/Auth/ControlComponent'
import AvatarChange from '@/components/Auth/AvatarChange'
import UserInfoModal from '@/components/Shared/UserInfoModal'
import NewThreadModal from '@/components/Forum/NewThreadModal'
import UpdateThreadModal from '@/components/Forum/UpdateThreadModal'
import DeleteThreadModal from '@/components/Forum/DeleteThreadModal'
import UpdateCommentModal from '@/components/Forum/UpdateCommentModal'
import DeleteCommentModal from '@/components/Forum/DeleteCommentModal'
import CommentOwnerInfo from '@/components/Forum/CommentOwnerInfo'


import { EmojiPickerPlugin } from 'vue-emoji-picker'
Vue.use(EmojiPickerPlugin)


/*import VueUploadMultipleImage from 'vue-upload-multiple-image'*/


/*Vue.component('vue-upload-multiple-image', VueUploadMultipleImage)*/
Vue.component('user-progress', userProgress)
Vue.component('app-login-modal', LoginModalComponent)
Vue.component('app-registration-modal', RegistrationModalComponent)
Vue.component('to-check-modal', ToCheckModal)
Vue.component('app-add-material', AddMaterial)
Vue.component('app-user-data-change', UserDataChange)
Vue.component('control-component', ControlComponent)
Vue.component('avatar-change', AvatarChange)
Vue.component('user-info-modal', UserInfoModal)
Vue.component('new-thread-modal', NewThreadModal)
Vue.component('update-thread-modal', UpdateThreadModal)
Vue.component('delete-thread-modal', DeleteThreadModal)
Vue.component('update-comment-modal', UpdateCommentModal)
Vue.component('delete-comment-modal', DeleteCommentModal)
Vue.component('comment-owner-info', CommentOwnerInfo)


/*Vue.config.productionTip = false*/

new Vue({
    render: h => h(App),
    router,
    store,
    created () {
        fb.initializeApp ({
            apiKey: "AIzaSyBeya0zNVdbl_qcpSG8rAonec1zex1zl-A",
            authDomain: "guitar-project-bim.firebaseapp.com",
            databaseURL: "https://guitar-project-bim.firebaseio.com",
            projectId: "guitar-project-bim",
            storageBucket: "guitar-project-bim.appspot.com",
            messagingSenderId: "309159197448",
            appId: "1:309159197448:web:d137f7526a9f5012"
        })
        fb.auth().onAuthStateChanged(user => {
            if (user) {
                this.$store.dispatch('autoLoginUser', user)
            }
        })
        this.$store.dispatch('fetchVideos')
        this.$store.dispatch('fetchLessonsToCheck')
        this.$store.dispatch('fetchUsers')
        this.$store.dispatch('fetchThreads')
        this.$store.dispatch('fetchComments')
    },

}).$mount('#app')
