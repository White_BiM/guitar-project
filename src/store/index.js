import Vue from "vue"
import Vuex from 'vuex'
import lessons from './lessons'
import bonuses from './bonuses'
import streams from './streams'
import user from './user'
import shared from './shared'
import forum from './forum'



Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        lessons, bonuses, streams, user, shared, forum
    }

})
