import * as fb from 'firebase'

/*class User{
    constructor(firstName, lastName, imageSrc, id){
        this.firstName = firstName
        this.lastName = lastName
        this.imageSrc = imageSrc
        this.id = id
    }
}*/
const newUserDataForStore = {}

function userDataFetching(storageSnapshot) {
    newUserDataForStore.firstName = storageSnapshot.val().firstName
    newUserDataForStore.lastName = storageSnapshot.val().lastName
    newUserDataForStore.imageSrc = storageSnapshot.val().imageSrc
    newUserDataForStore.id = storageSnapshot.val().id
    newUserDataForStore.userProgress = storageSnapshot.val().userProgress
    newUserDataForStore.userRank = storageSnapshot.val().userRank
    newUserDataForStore.tutorComment = storageSnapshot.val().tutorComment
    newUserDataForStore.threadsCount = storageSnapshot.val().threadsCount
    newUserDataForStore.commentsCount = storageSnapshot.val().commentsCount
}

export default {
    state: {
        user: null,
        userList: [],
        avatarList: []
    },
    mutations: {
        setUser(state, payload){
           state.user = payload
        },
        updateUserData(state, payload){
           const user = state.user
            user.firstName = payload.firstName
            user.lastName = payload.lastName
            user.imageSrc = payload.imageSrc
        },
        userListForStore(state, payload){
            state.userList = payload
        },
        avatarListForStore(state, payload){
            state.avatarList = payload
        }
    },

    actions: {

        async registerUser({commit}, {email, password}) {
            commit('clearError')
            commit('setLoading', true)

            function writeUserData(uid) {


                fb.database().ref('usersData/' + uid).set({
                    firstName: 'Нет Имени',
                    lastName: 'Нет Фамилии',
                    imageSrc: 'https://img2.freepng.ru/20180331/iuw/kisspng-user-profile-computer-software-internet-bot-user-5abf1fc8f2c2a6.7377880715224749529944.jpg',
                    id: uid,
                    userProgress: 1,
                    userRank: 1,
                    threadsCount: 0,
                    commentsCount: 0
                });
            }

            try {
                await fb.auth().createUserWithEmailAndPassword(email, password)
                const userId = fb.auth().currentUser.uid

                await writeUserData(userId)


                await fb.database().ref('/usersData/' + userId).once('value')
                    .then(function (snapshot) {
                        userDataFetching(snapshot)
                    })

                /*console.log(newUserDataForStore)*/

                commit('setUser', newUserDataForStore)
                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },

        async createUserData({commit}, {firstName, lastName, imageSrc}) {
            commit('clearError')
            commit('setLoading', true)

            function newUserData(firstName, lastName, imageSrc, id) {
                const postData = {
                    firstName,
                    lastName,
                    imageSrc,
                    id
                };

                return fb.database().ref('/usersData/' + id).update(postData);
            }

            try {
                const userId = fb.auth().currentUser.uid

                await newUserData(firstName, lastName, imageSrc, userId)

                await fb.database().ref('/usersData/' + userId).once('value')
                    .then(function (snapshot) {
                        userDataFetching(snapshot)
                    })

                commit('updateUserData', newUserDataForStore)
                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)

                throw error
            }
        },

        async loginUser({commit}, {email, password}) {
            commit('clearError')
            commit('setLoading', true)

            try {
                await fb.auth().signInWithEmailAndPassword(email, password)
                const userId = fb.auth().currentUser.uid

                await fb.database().ref('/usersData/' + userId).once('value')
                    .then(function (snapshot) {
                        userDataFetching(snapshot)
                    })

                commit('setUser', newUserDataForStore)
                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                /*console.log(error.message)*/
                throw error
            }
        },

        logOutUser({commit}) {
            fb.auth().signOut()
            commit('setUser', null)
        },

        async autoLoginUser({commit}) {
            commit('clearError')
            commit('setLoading', true)

            try {
                const userId = fb.auth().currentUser.uid
                await fb.database().ref('/usersData/' + userId).once('value')
                    .then(function (snapshot) {
                        userDataFetching(snapshot)
                    })
                console.log(newUserDataForStore)
                commit('setUser', newUserDataForStore)
                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },

        async lessonPassed({commit}, {ownerId, tutorComment, count}) {
            commit('clearError')
            commit('setLoading', true)
            const comment = {
                tutorComment
            }
            try {

                await fb.database().ref('/usersData/' + ownerId).update(comment);

                const thisUser = await fb.database().ref('/usersData/' + ownerId).once('value')
                const thisUserData = thisUser.val()
                thisUserData.userProgress = thisUserData.userProgress + count

                await fb.database().ref('/usersData/' + ownerId).update(thisUserData)


                const checkListVal = await fb.database().ref('/checkList/').once('value')

                checkListVal.forEach(function (itemSnapshot) {

                    if (itemSnapshot.val().ownerId === ownerId) {
                        /* console.log(itemSnapshot.key)*/
                        fb.database().ref('/checkList/' + itemSnapshot.key).remove()
                    }

                });

                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },

        async fetchUsers({commit}) {
            commit('clearError')
            commit('setLoading', true)

            try {
                const userListVal = await fb.database().ref('/usersData').once("value")
                const userList = userListVal.val()


                commit('userListForStore', userList)

            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }

        },

        async avatarList({commit}){
            commit('clearError')
            commit('setLoading', true)
            const avatarListSrc = []

            try {

               const avatarListObj = await fb.storage().ref('/avatars/').listAll()
               /* console.log(avatarListObj)*/
                const avatarList = avatarListObj.items
                /*console.log(avatarList)*/

                Object.keys(avatarList).forEach(key => {
                    const avatar = avatarList[key]
                    avatar.getDownloadURL().then(function(url) {
                        avatarListSrc.push(url)
                    })
                })
                /*console.log(avatarListSrc)*/

                commit('avatarListForStore', avatarListSrc)
                commit('setLoading', false)
            }catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        }
    },
    getters: {
        user(state) {
            return state.user
        },
        isUserLoggedIn(state) {
            return state.user !== null
        },
        userList(state){
            return state.userList
        },
        avatarList(state){
            return state.avatarList
        }

    }
}