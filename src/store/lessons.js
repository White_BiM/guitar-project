import * as fb from 'firebase'

function formatDate(date) {

    let dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    let yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    return dd + '.' + mm + '.' + yy;
}


class Video {
    constructor(title, description, videoSrc, imageSrc = '', text, type, number = 1, id = null) {
        this.title = title
        this.description = description
        this.videoSrc = videoSrc
        this.imageSrc = imageSrc
        this.text = text
        this.type = type
        this.number = number
        this.id = id
    }
}
class LessonForCheck {
    constructor(src, comment, ownerId, lessonId, lessonTitle, /*ownerFirstName, ownerLastName, */creationDate = '') {
        this.src = src
        this.comment = comment
        this.ownerId = ownerId
        this.lessonId = lessonId
        this.lessonTitle = lessonTitle
       /* this.ownerFirstName = ownerFirstName
        this.ownerLastName = ownerLastName*/
        this.creationDate = creationDate
    }
}


export default{
    state: {
        lessons: [],
        bonuses: [],
        streams: [],
        lessonsToCheck: []
    },
    mutations: {
        addVideo(state, payload){
            if (payload.type === 'Урок'){
                state.lessons.push(payload)
            } else if (payload.type === 'Бонус-трек'){
                state.bonuses.push(payload)
            }else if (payload.type === 'Стрим'){
                state.streams.push(payload)
            }
        },
        loadLessons(state, payload){
           state.lessons = payload
        },
        loadBonuses(state, payload){
            state.bonuses = payload
        },
        loadStreams(state, payload){
            state.streams = payload
        },
        fetchLessonsToCheckForStore(state, payload){
            state.lessonsToCheck = payload
        },
    },
    actions: {
        async addVideo ({commit}, payload){
            commit('clearError')
            commit('setLoading', true)

            const scheme = payload.image

            const number = 1

            try{
                const newVideo = new Video(
                    payload.title,
                    payload.description,
                    payload.videoSrc,
                    payload.imageSrc,
                    payload.text,
                    payload.type,
                    number
                    )

                let video = {}

                if (newVideo.type === 'Урок'){
                    video = await fb.database().ref('materials/lessons/').push(newVideo);
                } else if (newVideo.type === 'Бонус-трек'){
                    video = await fb.database().ref('materials/bonuses/').push(newVideo);
                }else if (newVideo.type === 'Стрим'){
                    video = await fb.database().ref('materials/streams/').push(newVideo);
                }

                let imageSrc = ''
                if (scheme!==null) {
                    const fileData = await fb.storage().ref(`schemes/` + video.key + `/${scheme.name}`).put(scheme)
                    imageSrc = await fileData.ref.getDownloadURL()
                }


                if (newVideo.type === 'Урок'){
                    await fb.database().ref('materials/lessons').child(video.key).update({
                        imageSrc
                    })
                } else if (newVideo.type === 'Бонус-трек'){
                    await fb.database().ref('materials/bonuses').child(video.key).update({
                        imageSrc
                    })
                }else if (newVideo.type === 'Стрим'){
                    await fb.database().ref('materials/streams').child(video.key).update({
                        imageSrc
                    })
                }
                commit ('addVideo',  {...newVideo,
                    id: video.key, imageSrc})
                commit('setLoading', false)
            }catch(error){
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },

         async fetchVideos({commit}){
            commit('clearError')
            commit('setLoading', true)

             function fetchVids(vids, resultArray) {
                 Object.keys(vids).forEach(key => {
                     const video = vids[key]
                     resultArray.push(
                         new Video(video.title, video.description, video.videoSrc, video.imageSrc, video.text, video.type, video.number, key)
                     )
                 })
             }
             function awaiting(awaitFB, resultArray, loadWay){
                 const fbVal = awaitFB
                 const videos = fbVal.val()
                 fetchVids(videos, resultArray)
                 commit (loadWay, resultArray)
             }
            const resultLessons = []
            const resultStreams = []
            const resultBonuses = []
            try{
                commit('setLoading', false)
                awaiting(await fb.database().ref(`/materials/lessons`).once('value'), resultLessons, ('loadLessons'))
                awaiting(await fb.database().ref(`/materials/bonuses`).once('value'), resultBonuses, ('loadBonuses'))
                awaiting(await fb.database().ref(`/materials/streams`).once('value'), resultStreams, ('loadStreams'))

            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

        async checkVideo ({commit}, payload){
            commit('clearError')
            commit('setLoading', true)

            const checkVideoDate = formatDate(new Date());


            const CheckLesson = new LessonForCheck(
                payload.checkSrc,
                payload.checkComment,
                payload.checkOwner,
                payload.lessonId,
                payload.lessonTitle
            )

            try {

                const userVal = await fb.database().ref('/usersData/' + payload.checkOwner).once('value')
                const user = userVal.val()

                /*CheckLesson.ownerFirstName = user.firstName
                CheckLesson.ownerLastName = user.lastName*/
                CheckLesson.creationDate = checkVideoDate

                await fb.database().ref('/checkList/').push(CheckLesson)
                commit('setLoading', false)

            }catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }

        },

        async fetchLessonsToCheck({commit}){
            commit('clearError')
            commit('setLoading', true)

            const LessonsToCheck = []

            try {
               const LessonsToCheckVal = await fb.database().ref(`/checkList/`).once('value')
                const CheckLessons = LessonsToCheckVal.val()


                if (CheckLessons !== null) {
                    Object.keys(CheckLessons).forEach(key => {
                        const lessonToCheck = CheckLessons[key]
                        LessonsToCheck.push(
                            new LessonForCheck(lessonToCheck.src, lessonToCheck.comment, lessonToCheck.ownerId, lessonToCheck.lessonId, lessonToCheck.lessonTitle, /*lessonToCheck.ownerFirstName , lessonToCheck.ownerLastName, */lessonToCheck.creationDate)
                        )
                    })
                }


               /* if (LessonsToCheck === undefined || LessonsToCheck === null) {
                    commit('fetchLessonsToCheckForStore', emptyArr)
                } else*/
                commit('fetchLessonsToCheckForStore', LessonsToCheck)
                commit('setLoading', false)
                console.log(CheckLessons)
            } catch(error){
            commit('setError', error.message)
            commit('setLoading', false)
            throw error
        }
        }


    },
    getters: {
        lessons (state) {
            return state.lessons
        },
        bonuses (state) {
            return state.bonuses
        },
        streams (state) {
            return state.streams
        },

        lessonById (state) {
            return videoId => {
                return state.lessons.find(video => video.id === videoId)
            }
        },
        bonusById (state) {
            return videoId => {
                return state.bonuses.find(video => video.id === videoId)
            }
        },
        streamById (state) {
            return videoId => {
                return state.streams.find(video => video.id === videoId)
            }
        },

        lessonsToCheck (state){
            return state.lessonsToCheck
        }
    }
}
