import * as fb from 'firebase'


function formatDate(date) {

    let dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    let yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    let hh = date.getHours();
    if (hh < 10) hh = '0' + hh;

    let mim = date.getMinutes();
    if (mim < 10) mim = '0' + mim;

    return dd + '.' + mm + '.' + yy + ' ' + hh + ':' + mim;
}



class Thread {
    constructor(title, content, ownerId, threadId = null, creationDate = '', commentsCount = 0, likes = []){
        this.title = title
        this.content = content
        this.ownerId = ownerId
        this.threadId = threadId
        this.creationDate = creationDate
        this.commentsCount = commentsCount
        this.likes = likes
    }
}

class Comment {
    constructor(comment, ownerId, threadId, creationDate = '', commentId = null) {
        this.comment = comment
        this.ownerId = ownerId
        this.threadId = threadId
        this.creationDate = creationDate
        this.commentId = commentId
    }
}


export default {
    state: {
        threads: [],
        comments: []
    },
    mutations: {
        addThread(state, payload){
            state.threads.push(payload)
        },
        loadThreads(state, payload){
            state.threads = payload
        },
        updateThread(state, {title, content, threadId}){
            const thread = state.threads.find(a => {
                return a.threadId === threadId
            })
            thread.title = title
            thread.content = content
        },
        addComment(state, payload){
            state.comments.push(payload)
        },
        loadComments(state, payload){
            state.comments = payload
        },
        updateComment(state, {comment, commentId}){
            const thisComment = state.comments.find(a => {
                return a.commentId === commentId
            })
            thisComment.comment = comment
        },
       /* updateLike(state, {threadId, userId, ownerId}){
            const thread = state.threads.find(a => {
                return a.threadId === threadId
            })
            const threadOwner = state.userList.find(a => {
                return a.id === ownerId
            })
            thread.likes = []
            thread.likes.push(userId)
            console.log(thread)
            threadOwner.likes = []
            threadOwner.likes.push(userId)
            console.log(threadOwner)
        }*/
    },

    actions: {
        async createNewThread ({commit}, {title, content, ownerId}){
            commit('clearError')
            commit('setLoading', true)

            const threadDate = formatDate(new Date());

            try {
                const newThread = new Thread(
                    title,
                    content,
                    ownerId,
                    null,
                    threadDate,
                    0,
                    []
                )

                const thread = await fb.database().ref('forum/').push(newThread);

                const threadId = thread.key

                await fb.database().ref('forum/').child(thread.key).update({
                    threadId
                })
                newThread.threadId = threadId


                const thisUser = await fb.database().ref('/usersData/' + ownerId).once('value')
                const thisUserData = thisUser.val()
                thisUserData.threadsCount++

                await fb.database().ref('/usersData/' + ownerId).update(thisUserData)


                commit ('addThread', newThread)

                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },

        async fetchThreads({commit}){
            commit('clearError')
            commit('setLoading', true)

            const resultThreads = []
            try{
                commit('setLoading', false)

                await fb.database().ref('/forum').once('value').then(function(snapshot) {
                    const allTreads = snapshot.val()
                    if (allTreads !== null) {
                        Object.keys(allTreads).forEach(key => {
                            const thread = allTreads[key]
                            resultThreads.push(
                                new Thread(thread.title, thread.content, thread.ownerId, thread.threadId, thread.creationDate, thread.commentsCount, thread.likes)
                            )
                        })
                    }
                });

                commit ('loadThreads', resultThreads)
                commit('setLoading', false)
            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

        async deleteThread({commit}, payload){
            commit('clearError')
            commit('setLoading', true)

            const threadId = payload.threadId
            const ownerId = payload.ownerId


            try {
                await fb.database().ref('forum/').child(threadId).remove()


                const commentsListVal = await fb.database().ref('/comments/').once('value')

                commentsListVal.forEach(function (itemSnapshot) {
                    if (itemSnapshot.val().threadId === threadId) {
                        /* console.log(itemSnapshot.key)*/
                        fb.database().ref('/comments/' + itemSnapshot.key).remove()
                    }
                });

                const thisUser = await fb.database().ref('/usersData/' + ownerId).once('value')
                const thisUserData = thisUser.val()
                thisUserData.threadsCount--

                await fb.database().ref('/usersData/' + ownerId).update(thisUserData)


                commit('setLoading', false)
            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

       async updateThread({commit}, {title, content, threadId}){
           commit('clearError')
           commit('setLoading', true)

           try {
               await fb.database().ref('forum/').child(threadId).update({
                   title, content
               })
               commit ('updateThread', {title, content, threadId})

               commit('setLoading', false)

           } catch(error){
               commit('setError', error.message)
               commit('setLoading', false)
               throw error
           }
       },

        async addComment({commit}, {comment, ownerId, threadId}){
            commit('clearError')
            commit('setLoading', true)

            const commentDate = formatDate(new Date());

            const newComment = new Comment(
                comment,
                ownerId,
                threadId,
                commentDate,
                null
            )

            try {
                const comment = await fb.database().ref('comments/').push(newComment);
                const commentId = comment.key

                await fb.database().ref('comments/').child(comment.key).update({
                    commentId
                })
                newComment.commentId = commentId


                const thisThread = await fb.database().ref('/forum/' + threadId).once('value')
                const thisThreadData = thisThread.val()
                thisThreadData.commentsCount++

                await fb.database().ref('/forum/' + threadId).update(thisThreadData)


                const thisUser = await fb.database().ref('/usersData/' + ownerId).once('value')
                const thisUserData = thisUser.val()
                thisUserData.commentsCount++

                await fb.database().ref('/usersData/' + ownerId).update(thisUserData)

                /*const checkListVal = await fb.database().ref('/checkList/').once('value')
                checkListVal.forEach(function (itemSnapshot) {

                    if (itemSnapshot.val().ownerId === ownerId) {
                        /!* console.log(itemSnapshot.key)*!/
                        fb.database().ref('/checkList/' + itemSnapshot.key).remove()
                    }

                });*/

                commit ('addComment', newComment)
                commit('setLoading', false)
            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

        async fetchComments({commit}){
            commit('clearError')
            commit('setLoading', true)

            const resultComments = []
            try{
                commit('setLoading', false)

                await fb.database().ref('/comments').once('value').then(function(snapshot) {
                    const allComments = snapshot.val()
                    if (allComments !== null) {
                        Object.keys(allComments).forEach(key => {
                            const comment = allComments[key]
                            resultComments.push(
                                new Comment(comment.comment, comment.ownerId, comment.threadId, comment.creationDate, comment.commentId)
                            )
                        })
                    }
                });

                commit ('loadComments', resultComments)
                commit('setLoading', false)
            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

        async updateComment({commit}, {comment, commentId}){
            commit('clearError')
            commit('setLoading', true)

            try {
                await fb.database().ref('comments/').child(commentId).update({
                    comment
                })
                commit ('updateComment', {comment, commentId})

                commit('setLoading', false)

            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

        async deleteComment({commit}, payload){
            commit('clearError')
            commit('setLoading', true)

            const commentId = payload.commentId
            const threadId = payload.threadId
            const ownerId = payload.ownerId

            try {
                await fb.database().ref('comments/').child(commentId).remove()


                const thisThread = await fb.database().ref('/forum/' + threadId).once('value')
                const thisThreadData = thisThread.val()
                thisThreadData.commentsCount--

                await fb.database().ref('/forum/' + threadId).update(thisThreadData)

                const thisUser = await fb.database().ref('/usersData/' + ownerId).once('value')
                const thisUserData = thisUser.val()
                thisUserData.commentsCount--

                await fb.database().ref('/usersData/' + ownerId).update(thisUserData)


                commit('setLoading', false)
            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },

        async addLike({commit}, {threadId, userId, ownerId}){
            commit('clearError')
            commit('setLoading', true)

            try {
                /*await fb.database().ref('forum/' + threadId + '/likes/' + userId).set(userId)*/
               /* await fb.database().ref('usersData/' + ownerId + '/likes/' + userId).set(userId)*/

                await fb.database().ref('forum/' + threadId).once('value').then(function(snapshot) {
                    const thread = snapshot.val()

                    if (!thread.likes) {
                        console.log(thread)
                        fb.database().ref('forum/' + threadId + '/likes/' + userId).set(userId)
                    } else
                        fb.database().ref('forum/' + threadId + '/likes/').once('value').then(function (snapshot) {
                            const allLikes = snapshot.val()
                            Object.keys(allLikes).forEach(key => {
                                const like = allLikes[key]
                                if (!allLikes.hasOwnProperty(userId)) {
                                    console.log(userId)
                                    fb.database().ref('forum/' + threadId + '/likes/' + userId).set(userId)
                                } else fb.database().ref('forum/' + threadId + '/likes/' + userId).remove()
                            })
                        });
                });

               /* await fb.database().ref('forum/' + threadId + '/likes/').once('value').then(function(snapshot) {
                    const allLikes = snapshot.val()
                    if (allLikes !== null) {
                        Object.keys(allLikes).forEach(key => {
                            const like = allLikes[key]
                            if (!allLikes.hasOwnProperty(userId)) {
                                console.log(userId)
                                fb.database().ref('forum/' + threadId + '/likes/' + userId).set(userId)
                            } else fb.database().ref('forum/' + threadId + '/likes/' + userId).remove()

                        })
                    }
                });
*/

               /* commit ('updateLike', {threadId, userId, ownerId})*/
                commit('setLoading', false)
            } catch(error){
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        }
    },

    getters:  {
        threads (state) {
            return state.threads
        },
        threadById (state) {
            return threadId => {
                return state.threads.find(video => video.threadId === threadId)
            }
        },
        comments (state) {
            return state.comments
        },
        commentById (state) {
            return commentId => {
                return state.comments.find(comment => comment.commentId === commentId)
            }
        },
    }
}